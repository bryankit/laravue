<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTTheme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_theme', function (Blueprint $table) {
            $table->integer('theme_no')->autoIncrement()->unsigned();
            $table->string('background_image_url', 3000)->nullable(false);;
            $table->string('button_image_url', 3000)->nullable(false);;
            $table->timestamp('ins_timestamp')->useCurrent();
            $table->timestamp('upd_timestamp')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_theme');
    }
}
