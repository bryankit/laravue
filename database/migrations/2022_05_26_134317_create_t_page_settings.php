<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTPageSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_page_settings', function (Blueprint $table) {
            $table->integer('category_no')->autoIncrement()->unsigned();
            $table->integer('store_no')->unsigned();
            $table->foreign('store_no')->references('store_no')->on('t_store');
            $table->enum('sort_type', ['amount', 'rate'])->nullable(false);
            $table->json('exempted_coupons_json')->nullable(false);
            $table->json('best_coupons_json')->nullable(false);
            $table->json('new_coupons_json')->nullable(false);
            $table->timestamp('ins_timestamp')->useCurrent();
            $table->timestamp('upd_timestamp')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_page_settings');
    }
}
