<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_store', function (Blueprint $table) {
            $table->integer('store_no', 11)->autoIncrement()->unsigned();
            $table->string('mall_id', 50)->nullable(false);
            $table->integer('shop_no')->nullable(false);
            $table->enum('is_all_displayed', ['T', 'F'])->nullable(false);
            $table->integer('theme_no')->unsigned();
            $table->foreign('theme_no')->references('theme_no')->on('t_theme');
            $table->timestamp('ins_timestamp')->useCurrent();
            $table->timestamp('upd_timestamp')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_store');
    }
}
